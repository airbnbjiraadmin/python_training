'''
Super basic example, does a search and prints the results.
'''
import requests
import json
import getpass
import sys

# Username and password terminal input
username = raw_input('Jira Username: ')
password = getpass.getpass()

# Put the endpoint here
url = 'https://jirarest-stage.airbnb.biz/rest/api/2/search?jql=project=ITXPT'

# Start a session
s = requests.Session()

# Make request
r = s.get(
        url=url,
        auth=(username, password),
        headers={'Content-Type': 'application/json'}
    )

status_code = r.status_code

# Prevents trying to run the loop with bad creds causing CAPTCHA lockout
if status_code == 401:
    sys.exit("Invalid credentials provided, please check username/password")

# Print results
print r.json()