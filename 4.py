'''
Loops over all service desks, and pulls thier names and IDs, as well as the name,
description, and help text of the request types. That data is then exported to a
CSV in the same folder the script was run.
'''
import requests
import json
import getpass
import sys
import datetime

# Username and password terminal input
username = raw_input('Jira Username: ')
password = getpass.getpass()

# Put the endpoint here
url = 'https://jirarest-stage.airbnb.biz/rest/servicedeskapi/servicedesk/'

# Start a session
s = requests.Session()

# Make request
r = s.get(
        url=url,
        auth=(username, password),
        headers={'Content-Type': 'application/json'}
    )

status_code = r.status_code

# Prevents trying to run the loop with bad creds causing CAPTCHA lockout
if status_code == 401:
    sys.exit("Invalid credentials provided, please check username/password")

servicedesk_lists = []

# Get service desk name and ID
for servicedesk in r.json().get("values"):
	servicedesk_lists.append([servicedesk.get("projectName"), servicedesk.get("id")])

servicedesk_requests = []

# Using service desk ID, get request types, their description, and their help text
for servicedesk in servicedesk_lists:
	r = s.get(
        url=url + servicedesk[1] + "/requesttype",
        headers={'Content-Type': 'application/json'}
	)

	for requesttype in r.json().get("values"):
		servicedesk_requests.append([servicedesk[0], requesttype.get("name"), 
            requesttype.get("description"), requesttype.get("helpText")])

# Print to CSV
with open('service-desk-requests-' + str(datetime.datetime.now().microsecond) + '.csv', 'a') as output_file:
    output_file.write("Service Desk Name, Request Type, Description, Help Text\n")
    for line in servicedesk_requests:
        output_file.write(line[0].encode("utf-8") + "| " + line[1].encode("utf-8") + "| " + line[2].encode("utf-8") + "| " + line[3].encode("utf-8") + "\n")
