'''
Grabs a list of all the service desks, prints their names and IDs.
'''
import requests
import json
import getpass
import sys

# Username and password terminal input
username = raw_input('Jira Username: ')
password = getpass.getpass()

# Put the endpoint here
url = 'https://jirarest-stage.airbnb.biz/rest/servicedeskapi/servicedesk/'

# Start a session
s = requests.Session()

# Make request
r = s.get(
        url=url,
        auth=(username, password),
        headers={'Content-Type': 'application/json'}
    )

status_code = r.status_code

# Prevents trying to run the loop with bad creds causing CAPTCHA lockout
if status_code == 401:
    sys.exit("Invalid credentials provided, please check username/password")

# Loops over the service desks, returns the name and ID.
for servicedesk in r.json().get("values"):
	print "Project name: " + servicedesk.get("projectName") + "\nService Desk id: " + servicedesk.get("id")